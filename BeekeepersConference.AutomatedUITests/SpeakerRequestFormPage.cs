using OpenQA.Selenium;

namespace BeekeepersConference.AutomatedUITests;

public class SpeakerRequestFormPage
{
    private readonly IWebDriver _webDriver;
    private const string Uri = "https://localhost:7160/SpeakerRequestForm";
    private IWebElement NameElement => _webDriver.FindElement(By.Id("Name"));
    private IWebElement CityElement => _webDriver.FindElement(By.Id("City"));
    private IWebElement EmailElement => _webDriver.FindElement(By.Id("Email"));
    private IWebElement PhoneElement => _webDriver.FindElement(By.Id("Phone"));
    private IWebElement ExperienceElement => _webDriver.FindElement(By.Id("Experience"));
    private IWebElement CreateElement => _webDriver.FindElement(By.Id("create"));
    
    
    public string Title => _webDriver.Title;
    public string Source => _webDriver.PageSource;
    public string PhoneErrorMessage => _webDriver.FindElement(By.Id("Phone-error")).Text;
    
    public SpeakerRequestFormPage(IWebDriver webdriver)
    {
        _webDriver = webdriver;
    }

    public void Navigate() => _webDriver.Navigate().GoToUrl(Uri);
    
    public void SendName(string name) => NameElement.SendKeys(name);
    public void SendCity(string city) => CityElement.SendKeys(city);
    public void SendEmail(string email) => EmailElement.SendKeys(email);
    public void SendPhone(string phone) => PhoneElement.SendKeys(phone);
    public void SendExperience(string expected) => ExperienceElement.SendKeys(expected);
    public void ClickCreate() => CreateElement.Click();
}