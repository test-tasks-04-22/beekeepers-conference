using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Xunit;

namespace BeekeepersConference.AutomatedUITests;

public class AutomatedUiTests : IDisposable
{
    private readonly IWebDriver _webDriver;
    private readonly BeekeeperRequestFormPage _beekeeper;
    private readonly SpeakerRequestFormPage _speaker;

    public AutomatedUiTests()
    {
        _webDriver = new ChromeDriver();
        _beekeeper = new BeekeeperRequestFormPage(_webDriver);
        _speaker = new SpeakerRequestFormPage(_webDriver);
    }
    
    [Fact]
    public void ReturnBeekeeperRequestFormView()
    {
        _beekeeper.Navigate();
        Assert.Equal("Заявка участника - BeekeepersConference", _beekeeper.Title);
        Assert.Contains("Придёте с пчёлами?", _beekeeper.Source);
    }    
    
    [Fact]
    public void ReturnSpeakerRequestFormView()
    {
        _speaker.Navigate();
        Assert.Equal("Заявка спикера - BeekeepersConference", _speaker.Title);
        Assert.Contains("Опыт пчеловодства в годах", _speaker.Source);
    }

    [Fact]
    public void BeekeeperEmptyRequiredField()
    {
        _beekeeper.Navigate();
        _beekeeper.SendName("Test");
        _beekeeper.ClickCreate();
        
        Assert.Equal("Поле обязательно для заполнения", _beekeeper.EmailErrorMessage);
    }    
    [Fact]
    public void SpeakerEmptyRequiredField()
    {
        _speaker.Navigate();
        _speaker.SendName("Test");
        _speaker.ClickCreate();
        
        Assert.Equal("Поле обязательно для заполнения", _speaker.PhoneErrorMessage);
    }

    [Fact]
    public void SuccessfulBeekeeperCreation()
    {
        _beekeeper.Navigate();
        _beekeeper.SendName("Вася Пупочкин");
        _beekeeper.SendCity("Верхняя Синячиха");
        _beekeeper.SendEmail("vasya@mail.ru");
        _beekeeper.SendPhone("+9090909090");
        _beekeeper.ClickCreate();
        
        Assert.Equal("Заявка принята - BeekeepersConference", _beekeeper.Title);
        Assert.Contains("Вася Пупочкин", _beekeeper.Source);
        Assert.Contains("Для ваших пчёл будет выделен дополнительный улей.", _beekeeper.Source);
    }    
    
    [Fact]
    public void SuccessfulSpeakerCreation()
    {
        _speaker.Navigate();
        _speaker.SendName("Гжегож Бженчишчикевич");
        _speaker.SendCity("Гданьск");
        _speaker.SendEmail("qwerty@mail.pl");
        _speaker.SendPhone("+123456788");
        _speaker.SendExperience("10");
        _speaker.ClickCreate();
        
        Assert.Equal("Заявка принята - BeekeepersConference", _speaker.Title);
        Assert.Contains("Гжегож Бженчишчикевич", _speaker.Source);
        Assert.Contains("Пчеловоды России с нетерпением ждут ваших бесценных советов", _speaker.Source);
    }

    public void Dispose()
    {
        _webDriver.Quit();
        _webDriver.Dispose();
    }
}