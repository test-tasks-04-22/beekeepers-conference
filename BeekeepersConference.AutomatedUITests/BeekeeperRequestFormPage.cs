using OpenQA.Selenium;

namespace BeekeepersConference.AutomatedUITests;

public class BeekeeperRequestFormPage
{
    private readonly IWebDriver _webDriver;
    private const string Uri = "https://localhost:7160/BeekeeperRequestForm";
    private IWebElement NameElement => _webDriver.FindElement(By.Id("Name"));
    private IWebElement CityElement => _webDriver.FindElement(By.Id("City"));
    private IWebElement EmailElement => _webDriver.FindElement(By.Id("Email"));
    private IWebElement PhoneElement => _webDriver.FindElement(By.Id("Phone"));
    private IWebElement CreateElement => _webDriver.FindElement(By.Id("create"));

    public string Title => _webDriver.Title;
    public string Source => _webDriver.PageSource;
    public string EmailErrorMessage => _webDriver.FindElement(By.Id("Email-error")).Text;

    public BeekeeperRequestFormPage(IWebDriver webdriver)
    {
        _webDriver = webdriver;
    }

    public void Navigate() => _webDriver.Navigate().GoToUrl(Uri);

    public void SendName(string name) => NameElement.SendKeys(name);
    public void SendCity(string city) => CityElement.SendKeys(city);
    public void SendEmail(string email) => EmailElement.SendKeys(email);
    public void SendPhone(string phone) => PhoneElement.SendKeys(phone);
    public void ClickCreate() => CreateElement.Click();
}