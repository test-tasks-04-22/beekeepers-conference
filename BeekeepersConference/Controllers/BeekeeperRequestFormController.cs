using BeekeepersConference.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeekeepersConference.Controllers;

public class BeekeeperRequestFormController : Controller
{
    // GET
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(Beekeeper beekeeper)
    {
        return ModelState.IsValid ? View("Accepted", beekeeper) : View();
    }
}