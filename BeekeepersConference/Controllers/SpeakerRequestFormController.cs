using BeekeepersConference.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeekeepersConference.Controllers;

public class SpeakerRequestFormController : Controller
{
    // GET
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(Speaker speaker)
    {
        return ModelState.IsValid ? View("Accepted", speaker) : View();
    }
}