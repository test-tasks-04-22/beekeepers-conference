﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using BeekeepersConference.Models;

namespace BeekeepersConference.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        int hour = DateTime.Now.Hour;
        const string user = "дорогой пчеловод";
        ViewBag.Hello = hour switch
        {
            < 6 => $"Доброй ночи, {user} :)",
            < 12 => $"Доброе утро, {user} :)",
            >= 18 => $"Добрый вечер, {user} :)",
            _ => $"Добрый день, {user} :)"
        };
        return View();
    }
    

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}