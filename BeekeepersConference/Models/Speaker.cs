using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BeekeepersConference.Models;

public class Speaker
{
    [Required(ErrorMessage = "Анонимным участникам у нас не рады")]
    [DisplayName("Имя, фамилия")]
    public string Name { get; set; } = string.Empty;
    
    [Required(ErrorMessage = "Нам важно знать, откуда вы")]
    [DisplayName("Город")]
    public string City { get; set; } = string.Empty;
    
    [Required(ErrorMessage = "Поле обязательно для заполнения")]
    [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage="Это не email")]
    [DisplayName("Email")]
    public string Email { get; set; } = string.Empty;
    
    [Required(ErrorMessage = "Поле обязательно для заполнения")]
    [RegularExpression(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$", ErrorMessage="Это не номер телефона")]
    [DisplayName("Номер телефона")]
    public string Phone { get; set; } = string.Empty;

    [Required(ErrorMessage = "Поле обязательно для заполнения")]
    [DisplayName("Опыт пчеловодства в годах")]
    public int Experience { get; set; } 
}